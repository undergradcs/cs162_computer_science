/*========================================================
Name: student.cpp
Description: Contains the class implementation for student
Programmer: Medina Lamkin
Date: 05/18/17
========================================================*/
#define _CRT_SECURE_NO_WARNINGS

//including the student header file
#include "student.h"

Student::Student()
{
	gpa = NULL;
	name = NULL;
};

Student::Student(float gpa, char* name)
{

	if (gpa < 0 || gpa > 4)
		this->gpa = 0;
	else
		this->gpa = gpa;

	//allocates dynamic memory to store the name
	//copies the pointer into the private member variable
	this->name = new char[strlen(name) + 1];
	strcpy(this->name, name);

};

Student::~Student() {
	//deallocates the memory the pointer points to (if any)
	//sets pointer to NULL
	if (name != NULL) {
		delete name;
		name = NULL;
	}
};

void Student::changeName(const char* newName)
{
	//if the pointer isn't NULL, the memory it points to is deallocated,
	//and the pointer is set to NULL, then new memory is allocated and the
	// content is copied into this new memory location
	if (name != NULL) {
		delete name;
		name = NULL;

		name = new char[strlen(newName) + 1];
		strcpy(name, newName);
	}
};

bool Student::isEqualTo(const char* name) const
{
	if (this->name == NULL)
		return false;

	//strcmp returns 0 is the strings are the same
	else if (strcmp(this->name, name) == 0)
		return true;

	else
		return false;
}

void Student::print() const
{
	cout << name << showpoint << setprecision(2)
		<< setfill('0') << "(" << gpa << ")";
};