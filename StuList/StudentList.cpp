/*========================================================
Name: studentlist.cpp
Description: Contains the class implementation for
			 studentlist
Programmer: Medina Lamkin
Date: 05/18/17
========================================================*/
//including the list header file
#include "studentlist.h"

StudentList::StudentList()
{
	//initializes all pointers in students to NULL
	for (int index = 0; index < MAX_STUDENTS; index++)
		students[index] = NULL;
};

StudentList::~StudentList()
{
	//if any pointer is not NULL, the memory it points to is
	//deallocated, and the pointer is set to NULL
	for (int index = 0; index < MAX_STUDENTS; index++)
		if (students[index] != NULL) {
			delete students[index];
			students[index] = NULL;
		}
};

bool StudentList::empty() const
{
	//checks if any pointer doesn't contain NULL; 
	//if any value besides NULL is found, list is not empty
	for (int index = 0; index < MAX_STUDENTS; index++)
		if (students[index] != NULL)
			return false;

	return true;
};

bool StudentList::full() const
{
	//checks if any pointer contains a NULL; if one does, list not full 
	for (int index = 0; index < MAX_STUDENTS; index++)
		if (students[index] == NULL)
			return false;

	return true;
};

bool StudentList::insert(Student* student)
{
	//a loop that finds the first NULL pointer and inserts the student
	for (int index = 0; index < MAX_STUDENTS; index++)
		if (students[index] == NULL) {
			students[index] = student;
			return true;
		}
	return false;
};

//removes the item from the list, if it is present
//returns true if the item has been removed, false if not
bool StudentList::remove(const char* name)
{
	//a loop to check if a student is in the list and replaces it if they are
	for (int index = 0; index < MAX_STUDENTS; index++) {

		if (students[index] == NULL)
			continue;

		else if (students[index]->isEqualTo(name)) {
			delete students[index];
			students[index] = NULL;
			return true;
		}
	}
	return false;
};

//prints all the strings in items[]
void StudentList::printList() const
{
	//a loop that prints all the names and gpas stored in the list
	for (int index = 0; index < MAX_STUDENTS; index++)
		if (students[index] != NULL) {
			students[index]->print();
			cout << ", ";
		}
	cout << "\b\b " << endl;
};