/*========================================================
Name: studentlist.h
Description: Contains the class definition for studentlist
Programmer: Medina Lamkin
Date: 05/18/17
========================================================*/
#pragma once

//pre-processor directives
#include <string>
#include <iostream>
#include "Student.h"

using namespace std;

//constant specifying the maximum number of students
const int MAX_STUDENTS = 10;

class StudentList {
public:
	//The default constructor; initiallizes all the array pointers to NULL
	StudentList();

	//The default destructor;frees up any the space
	//pointed to by the pointers in the array.
	~StudentList();

	// inserts the given student pointer into the array in the first NULL location
	//returns true if pointer was inserted, false otherwise
	bool insert(Student* student);

	//Returns true if list is empty (all NULL pointers); false otherwise
	bool empty() const;

	//Returns true if list is full; false otherwise
	bool full() const;

	//removes the student with the given name from the array
	//frees up the memory and sets the pointer to NULL.
	//Returns true if successful, false otherwise.
	bool remove(const char* name);

	//Prints the student's names and gpa in brackets, with a newline at the end
	void printList() const;

private:
	Student* students[MAX_STUDENTS];
};
