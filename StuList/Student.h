/*========================================================
Name: student.h
Description: Contains the class definition for student
Programmer: Medina Lamkin
Date: 05/18/17
========================================================*/
#pragma once

//pre-processor directives
#include <iostream>
#include <cstring>
#include <iomanip>

using namespace std;

class Student {
public:
	//default constructor; everything set to NULL
	Student();

	//constructor that checks if GPA is between 0 and 4;
	//if out of bounds it sets  it 0.
	//dynamically allocate just enough space for the name
	//and copies the given name there.
	Student(float gpa, char* name);

	//A destructor that frees up the space allocated for the name
	//(if space was allocated).
	~Student();

	//Changes studentís name (deallocates then reallocates memory)
	void changeName(const char* newName);

	//returns true if the studentís name is equal to the given name.
	//returns false if the names arenít equal.
	bool isEqualTo(const char* name) const;

	//prints out the studentís name, followed by the gpa in().
	void print() const;

private:
	float gpa;
	//pointer pointing to dynamically allocated memory storing the student's name
	char* name;
};