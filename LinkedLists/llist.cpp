/*=================================================================
Name: llist.cpp
Description: contains the class implementation for LinkedList
Programmer: Medina Lamkin
Date: 05/31/17
=================================================================*/
//pre-processor directives
#include "llist.h"

LinkedList::LinkedList()
{
	head = NULL;
};


LinkedList::~LinkedList()
{
	if (head != NULL) {
		Node* current = head;
		Node* previous = NULL;

		while (current != NULL) {
			previous = current;
			current = current->nextNode;
			delete previous;
		}
		delete current;
	}
};


bool LinkedList::insert(string name)
{
	//The list is empty
	if (head == NULL) {
		Node* newNode = new Node;
		newNode->name = name;
		newNode->nextNode = NULL;

		head = newNode;
		return true;
	}

	//Name goes first
	else if (name < head->name) {
		Node* newNode = new Node;
		newNode->name = name;
		newNode->nextNode = NULL;

		newNode->nextNode = head;
		head = newNode;
		return true;
	}

	//goes through the list until either the end is reached or
	//until the name comes after one alredy stored/ is the same 
	else {
		Node* current = head;
		Node* previous = NULL;

		while (current != NULL && name >= current->name) {
			previous = current;
			current = current->nextNode;
		}

		//deletes the newNode if it is a duplicate; return false
		if (name == previous->name) {
			return false;
		}

		//allocates memory for the name to store it in the list;
		//rearranges pointers to keep list correctly in order.
		Node* newNode = new Node;
		newNode->name = name;
		newNode->nextNode = NULL;

		newNode->nextNode = previous->nextNode;
		previous->nextNode = newNode;
		return true;
	}
};


bool LinkedList::remove(string name) {
	//If the list is empty
	if (head == NULL)
		return false;

	//if the first name is to be deleted
	if (name == head->name) {
		Node* temporaryPointer = head;
		head = head->nextNode;
		delete temporaryPointer;
		return true;
	}

	//if any subsequent to the first name is to be deleted
	Node* nodeToDelete = head->nextNode;
	Node* previous = head;

	while (nodeToDelete != NULL && nodeToDelete->name < name) {
		previous = nodeToDelete;
		nodeToDelete = nodeToDelete->nextNode;
	}

	if (nodeToDelete != NULL && name == nodeToDelete->name) {
		previous->nextNode = nodeToDelete->nextNode;
		delete nodeToDelete;
		return true;
	}
	return false;
};


void LinkedList::print() const {
	//name content of each node will be printed until current 
	//points to NULL (reaches end of list)
	if (head != NULL) {
		Node* current = head;
		while (current != NULL) {
			cout << current->name << ", ";
			current = current->nextNode;
		}
		cout << "\b\b " << endl;
	}
};


int LinkedList::count() const {
	//if the list is empty
	if (head == NULL)
		return 0;

	//counting names when list is not empty
	Node* current = head;
	int numberOfNodes = 0;

	while (current != NULL) {
		current = current->nextNode;
		numberOfNodes++;
	}
	return numberOfNodes;
};


int LinkedList::find(string name) const {
	//if the list is empty
	if (head == NULL)
		return 0;

	Node* current = head;
	int nodePosition = 1;

	//searches the list for the given name, but stops searching 
	//once the end of the list is reached, or if the name in the list
	//comes after the given name, or if the names are the same 
	while (current != NULL && current->name < name) {
		current = current->nextNode;
		nodePosition++;
	}
	if (current != NULL && current->name == name)
		return nodePosition;

	return 0;
};


void LinkedList::removeAll() {
	if (head != NULL) {
		Node* current = head;
		Node* previous = NULL;
		head = NULL;

		while (current != NULL) {
			previous = current;
			current = current->nextNode;
			delete previous;
		}
	}
};


LinkedList* LinkedList::duplicate() const {
	//new head pointer set to NULL
	LinkedList* duplicateList = new LinkedList;

	//if the linked list is empty
	if (head == NULL)
		return duplicateList;

	//pointer to traverse the original list
	Node* currentOriginal = head;

	//pointers to the current and previous item being duplicated in duplicate list
	Node* currentDuplicate = duplicateList->head;

	//copying the first name to the list
	Node* newNode = new Node;

	duplicateList->head = newNode;
	newNode->name = currentOriginal->name;
	newNode->nextNode = NULL;

	currentOriginal = currentOriginal->nextNode;

	currentDuplicate = newNode;

	//copying all subsequent names
	while (currentOriginal != NULL) {
		Node* newNode = new Node;

		currentDuplicate->nextNode = newNode;
		newNode->name = currentOriginal->name;
		newNode->nextNode = NULL;

		currentDuplicate = currentDuplicate->nextNode;
		currentOriginal = currentOriginal->nextNode;
	}
	return duplicateList;
};


void LinkedList::printReverse() const {
	//base case
	if (head == NULL)
		return;

	//general case
	printReverse(head);
	cout << "\b\b " << endl;
};


void LinkedList::printReverse(Node* next) const {
	if (next != NULL)
		printReverse(next->nextNode);

	if (next == NULL)
		return;

	cout << next->name << ", ";
	return;
};