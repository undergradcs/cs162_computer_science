/*=================================================================
Name: llist.h
Description: contains the class definition LinkedList and the
struct Node
Programmer: Medina Lamkin
Date: 05/31/17
=================================================================*/
#pragma once

//pre-processor directives
#include <iostream>
#include <string>

using namespace std;

//Each Node will store a name and a pointer to the next name
struct Node {
	string name;
	Node* nextNode;
};

class LinkedList {
public:
	//Default constructor
	LinkedList();

	//Default destructor
	~LinkedList();

	//Inserts the given string in the list (in alphabetic order).
	//Returns true if successful, false otherwise. 
	//No duplicates are allowed.
	bool insert(string name);

	//removes the given string from the list.
	//Returns true if successful, false otherwise.
	bool remove(string name);

	//prints the list in order(one line, comma delimited)
	void print() const;

	//returns a count of the number of strings in the list.
	int count() const;

	//Returns the given strings position in the list.
	//Returns 0 if the item is not in the list.
	int find(string name) const;

	//removes all strings from the list
	void removeAll();

	// makes an exact copy of the list,
	//returning a pointer to the new list.
	LinkedList* duplicate() const;

	//prints list in reverse order
	void printReverse() const;

private:
	Node* head;
	void printReverse(Node* next) const;
};