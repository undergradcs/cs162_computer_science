/*=================================================================
Name: LinkedListTest.cpp
Description: tests the LinkedList
Programmer: Medina Lamkin
Date: 05/31/17
=================================================================*/
//pre-processor directives
#include <iostream>
#include <string>
#include "llist.h"

using namespace std;

int main() {
	LinkedList* List = new LinkedList;

	List->insert("Bill");
	List->print();
	List->insert("Adam");
	List->print();
	List->insert("Zorro");
	List->print();
	List->insert("Kelly");
	List->print();
	List->insert("Chris");
	List->print();
	List->insert("Kelly");
	List->print();

	cout << "The total number of names currently in the list: "
		<< List->count() << endl;

	LinkedList* duplicateList = List->duplicate();

	List->remove("Charlie");
	List->print();
	List->remove("Becky");
	List->print();
	List->remove("Zorro");
	List->print();
	List->remove("Adam");
	List->print();
	List->remove("Kelly");
	List->print();

	duplicateList->insert("Sam");

	cout << "The total number of names currently in the list: "
		<< duplicateList->count() << endl;

	cout << "George is at " << duplicateList->find("George") << endl;
	cout << "Adam is at " << duplicateList->find("Adam") << endl;
	cout << "Kelly is at " << duplicateList->find("Kelly") << endl;
	cout << "Zorro is at " << duplicateList->find("Zorro") << endl;

	duplicateList->~LinkedList();

	cout << "DONE" << endl;

	return 0;
}